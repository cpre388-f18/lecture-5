package edu.iastate.jmay.lecture5_threads;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private int i = 0;
    private TextView asyncTextView;
    private TextView threadTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        asyncTextView = findViewById(R.id.asyncTaskTextView);
        threadTextView = findViewById(R.id.threadTextView);
    }

    public void onCounterButtonClick(View v) {
        Button counterButton = findViewById(R.id.counterButton);
        i++;
        counterButton.setText(String.format(Locale.getDefault(), "%s", i));
    }

    // --- RUN ON MAIN THREAD ---
    public void onMainThreadButtonClick(View v) throws InterruptedException {
        TextView mainThreadTextView = findViewById(R.id.mainThreadTextView);
        mainThreadTextView.setText(R.string.doing);
        Thread.sleep(10000);
        mainThreadTextView.setText(R.string.done);
    }

    // --- RUN IN ASYNCTASK ---
    public void onAsyncTaskButtonClick(View v) {
        // The following line was the bug encountered during lecture.  The generic class causes
        // a cast exception to Void[] when calling .execute().
//        AsyncTask newTask = new MyTask();
        // The following definition would be valid because the class datatype arguments match with
        // MyTask.
//        AsyncTask<Void, Integer, Void> newTask = new MyTask();
        // This is a concise way to do it.
        MyTask newTask = new MyTask();
        newTask.execute();
    }

    private class MyTask extends AsyncTask<Void, Integer, Void> {
        // Runs on a new thread
        @Override
        protected Void doInBackground(Void... voids) {
            for (int i = 0; i < 10; i++) {
                publishProgress(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        // Runs on main thread
        @Override
        protected void onProgressUpdate(Integer... values) {
            asyncTextView.setText(String.format(Locale.getDefault(), "%d", values[0]));
        }

        // Runs on main thread
        @Override
        protected void onPostExecute(Void v) {
            asyncTextView.setText(R.string.done);
        }
    }


    // --- RUN IN THREAD ---
    public void onThreadButtonClick(View v) {
        Thread newThread = new MyThread();
        // Note that we are calling .start(), not .run().
        newThread.start();
    }

    private class MyThread extends Thread {
        // This runs on a new thread
        @Override
        public void run() {
            UpdateThreadTextViewRunnable updater =
                    new UpdateThreadTextViewRunnable(getText(R.string.doing));
            threadTextView.post(updater);

            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                updater.setMessage(Integer.toString(i));
                threadTextView.post(updater);
            }

            updater.setMessage(getText(R.string.done));
            threadTextView.post(updater);
        }
    }

    private class UpdateThreadTextViewRunnable implements Runnable {
        private CharSequence message;

        UpdateThreadTextViewRunnable(CharSequence message) {
            this.message = message;
        }

        public void setMessage(CharSequence message) {
            this.message = message;
        }

        // This runs on the main thread, because it's passed to View.post(Runnable).
        @Override
        public void run() {
            threadTextView.setText(message);
        }
    }
}
