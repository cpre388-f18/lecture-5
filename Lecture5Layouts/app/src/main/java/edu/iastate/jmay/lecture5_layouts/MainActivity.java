package edu.iastate.jmay.lecture5_layouts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private int i = 0;
    private static final String KEY_I = "i";
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Notice how nothing special is done to load the landscape layout.
        setContentView(R.layout.activity_main);

        // This is all review for restoring a state.
        textView = findViewById(R.id.textView);
        if (savedInstanceState != null) {
            i = savedInstanceState.getInt(KEY_I);
        }
        textView.setText(String.format(Locale.getDefault(), "%d", i));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_I, i);
    }

    // This event handler is called by both layouts, because both buttons have this event handler
    // set for onClick.
    public void onButtonClick(View v) {
        i++;
        textView.setText(String.format(Locale.getDefault(), "%d", i));
    }
}
